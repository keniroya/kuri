#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "my_func.h"

int main()
{
  //FILE *fp;
  //char *fname = "./word.txt";
  char str[MAX_INPUT_LENGTH];
  char sNow[MAX_INPUT_LENGTH];
  char **words;
  char ***gram;
  char ***dic1;
  int n, i, j, nDic1, nGram, wNow, pNow, word;

  // 文の読み込み
  //if((fp = fopen(fname, "r")) == NULL){
  //printf("%sファイルが開けません¥n", fname);
  //return (-1);
  //}
  //fgets(str, MAX_INPUT_LENGTH, fp);
  strcpy(str, "the child runs quickly to the large house . end");
  words = my_split_str_blanks(str, &n);
  //fclose(fp);
  n--;
  
  /*
  for (i = 0; i < n; i++) {
    fprintf(stderr, "%dつめの要素 : %s\n", i+1, words[i]);
  }
  */
  
  nDic1=0;
  dic1 = scan_dictionary("dic1.txt", &nDic1);
  dic1 = (char ***)realloc(dic1, sizeof(char **)*(++nDic1));
  dic1[nDic1-1] = (char **) malloc(sizeof(char *) * 2);
  dic1[nDic1-1][0] = (char *) malloc(sizeof(char) * 128);
  dic1[nDic1-1][1] = (char *) malloc(sizeof(char) * 128);
  strcpy(dic1[nDic1-1][0], ".");
  strcpy(dic1[nDic1-1][1], "end");
  
  /*
  for (i = 0; i < nDic1; i++) {
    fprintf(stderr, "%dつめの辞書 : %s -> %s\n", i+1, dic1[i][0], dic1[i][1]);
  }
  */

  nGram=0;
  gram = scan_dictionary("gram.txt", &nGram);
 
  /*
  for (i = 0; i < nGram; i++) {
    fprintf(stderr, "%dつめの辞書 : %s -> %s\n", i+1, gram[i][0], gram[i][1]);
  }
  */

  // ここから
  pNow = 0;
  word = 1;
  fprintf(stderr, "%-9s \n", "");
  fprintf(stderr, "%-9s \n", " |");
  fprintf(stderr, "%-9s \n\n", "start");
  while(pNow < n){
    wNow = 0; // 文章の単語数
    strcpy(sNow, "start"); // 今の品詞
    if(word == 1){
      fprintf(stderr, "%-9s ", "");
    }else if(word == 0){
      fprintf(stderr, "%-9s ", " |");
    }else{
      fprintf(stderr, "%-9s ", sNow);
    }
    //fprintf(stderr, "%d,%s\n", wNow, "");
    while(wNow < n){
      for(i = 0; i < nGram; i++){
	if(strcmp( gram[i][0], sNow ) == 0){
	  for(j = 0; j < nDic1; j++){
	    if(strcmp( words[wNow], dic1[j][0] ) == 0){
	      if(strcmp( gram[i][1], dic1[j][1] ) == 0){
		strcpy(sNow, dic1[j][1]);
		if(word == 1){
		  fprintf(stderr, "%-9s ", dic1[j][0]);
		}else if(word == 0){
		  fprintf(stderr, "%-9s ", " |");
		}else{
		  fprintf(stderr, "%-9s ", sNow);
		}
		if(pNow == wNow++){
		  if(word == -1){
		    pNow++;
		    fprintf(stderr, "\n");
		    word=1;
		  }else{
		    word--;
		  }
		  wNow = n;
		  fprintf(stderr, "\n");
		}
		j = nDic1;
		i = nGram;
	      }
	    }
	  }
	}
      }
    }
  }
  
  for (i = 0; i < nGram; i++) {
    for (j = 0; j < 2; j++) {
      free(gram[i][j]);
    }
    free(gram[i]);
  }
  free(gram);
  
  for (i = 0; i < nDic1; i++) {
    for (j = 0; j < 2; j++) {
      free(dic1[i][j]);
    }
    free(dic1[i]);
  }
  free(dic1);
  
  for (i = 0; i < n; i++) {
    free(words[i]);
  }
  free(words);

  return 0;
}
