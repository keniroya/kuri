#include "my_func.h"

char **my_split_str_blanks(char *str_org, int *size_p)
{
  int size, count, len, i;
  char *str, *str_p, *start_p;
  char **array;

  len = strlen(str_org);
  str = (char *) malloc(sizeof(char) * (len + 1));
  strncpy(str, str_org, len);   // 元文字列を改変する(改行をいじる)ので
  //                               コピーを用意

  for (i = 0; i < strlen(str); i++) {   // 文字列中の改行等は空行扱い
    if ((str[i] == '\n') || (str[i] == '\t')) {
      str[i] = ' ';
    }
  }

  // 基本的に同じルーチンを2度回している
  // 1度め : サイズ測定(とそれにみあった領域確保)

  str_p = str;
  while (*str_p == ' ') {       // 文字列先頭の空白は読み飛ばし
    str_p++;
  }
  start_p = str_p;

  count = 0;
  while (*str_p != '\0') {
    while ((*str_p != ' ') && (*str_p != '\0')) {       // 空白は読み飛ばし
      str_p++;
    }
    count++;
    while (*str_p == ' ') {     // 空白でない部分の読みだし
      str_p++;
    }
  }
  size = count;                 // 要素が幾つあったかカウントして
  array = (char **) malloc(sizeof(char *) * size);      // その数だけ領域確保

  // 2度め : 確保した領域にデータをコピー

  count = 0;
  str_p = start_p;              // 文字列先頭の空白は読み飛ばし
  while (*str_p != '\0') {
    len = 0;
    start_p = str_p;
    while ((*str_p != ' ') && (*str_p != '\0')) {       // 要素の長さ確認をして
      str_p++;
      len++;
    }                           // その長さだけ領域確保
    array[count] = (char *) malloc(sizeof(char) * (len + 1));
    strncpy(array[count], start_p, len);        // その長さ分だけコピー
    array[count][len] = '\0';   // 文字列末尾処理
    count++;
    while (*str_p == ' ') {     // 空白は読み飛ばし
      str_p++;
    }
  }

  /*
     fprintf(stderr, "CHK; size : %d\n", size);
     for(i = 0; i < size; i++){
     fprintf(stderr, "CHK; %s\n", array[i]);
     }
   */

  *size_p = size;
  return (array);
}

char **my_split_str_plus(char *str_org, int *size_p)
{
  int size, count, len, i;
  char *str, *str_p, *start_p;
  char **array;

  len = strlen(str_org);
  str = (char *) malloc(sizeof(char) * (len + 1));
  strncpy(str, str_org, len);   // 元文字列を改変する(改行をいじる)ので
  //                               コピーを用意

  for (i = 0; i < strlen(str); i++) {   // 文字列中の改行等は空行扱い
    if ((str[i] == '\n') || (str[i] == '\t')) {
      str[i] = ' ';
    }
  }

  // 基本的に同じルーチンを2度回している
  // 1度め : サイズ測定(とそれにみあった領域確保)

  str_p = str;
  while (*str_p == ' ') {       // 文字列先頭の空白は読み飛ばし
    str_p++;
  }
  start_p = str_p;

  count = 0;
  while (*str_p != '\0') {
    while ((*str_p != ' ') && (*str_p != '+') && (*str_p != '\0')) {       // 空白は読み飛ばし
      str_p++;
    }
    count++;
    while (*str_p == '+') {     // +でない部分の読みだし
      str_p++;
    }
  }
  size = count;                 // 要素が幾つあったかカウントして
  array = (char **) malloc(sizeof(char *) * size);      // その数だけ領域確保

  // 2度め : 確保した領域にデータをコピー

  count = 0;
  str_p = start_p;              // 文字列先頭の空白は読み飛ばし
  while (*str_p != '\0') {
    len = 0;
    start_p = str_p;
    while ((*str_p != ' ') && (*str_p != '+') && (*str_p != '\0')) {       // 要素の長さ確認をして
      str_p++;
      len++;
    }                           // その長さだけ領域確保
    array[count] = (char *) malloc(sizeof(char) * (len + 1));
    strncpy(array[count], start_p, len);        // その長さ分だけコピー
    array[count][len] = '\0';   // 文字列末尾処理
    count++;
    while (*str_p == '+') {     // 空白は読み飛ばし
      str_p++;
    }
  }

  *size_p = size;
  return (array);
}

char ***scan_dictionary(char *fname, int *col)
{
  FILE *fp;
  char str[MAX_INPUT_LENGTH];
  char ***words;
  int n,i=0;
  
  if((fp = fopen(fname, "r")) == NULL){
    printf("%sファイルが開けません¥n", fname);
    return NULL;
  }
  while(fgets(str, MAX_INPUT_LENGTH, fp) != NULL){
    (*col)++;
  }
  fclose(fp);

  words = (char ***) malloc(sizeof(char **) * (*col));
  
  if((fp = fopen(fname, "r")) == NULL){
    printf("%sファイルが開けません¥n", fname);
    return NULL;
  }
  while(fgets(str, MAX_INPUT_LENGTH, fp) != NULL){
    words[i++] = my_split_str_blanks(str, &n);
  }

  return words;
}
