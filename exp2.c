#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "my_func.h"
  
int main()
{
  //FILE *fp;
  //char *fname = "./word.txt";
  char str[MAX_INPUT_LENGTH];
  char **words;
  char ***gram;
  char ***gram2;
  char ***matrix;
  char ***dic1;
  int n, i, j, k, l, m, nDic1, nGram;

  // 文の読み込み
  //if((fp = fopen(fname, "r")) == NULL){
  //printf("%sファイルが開けません¥n", fname);
  //return (-1);
  //}
  //fgets(str, MAX_INPUT_LENGTH, fp);
  strcpy(str, "the child runs quickly to the large house .");
  words = my_split_str_blanks(str, &n);
  //fclose(fp);
  n--;

  /*
  for (i = 0; i < n; i++) {
    fprintf(stderr, "%dつめの要素 : %s\n", i+1, words[i]);
  }
  */
  
  nDic1=0;
  dic1 = scan_dictionary("dic1.txt", &nDic1);
  
  /*
  for (i = 0; i < nDic1; i++) {
    fprintf(stderr, "%dつめの辞書 : %s -> %s\n", i+1, dic1[i][0], dic1[i][1]);
  }
  */

  nGram=0;
  gram = scan_dictionary("gram2.txt", &nGram);

  int nGram2[nGram];
  gram2 = (char ***) malloc(sizeof(char **) * (nGram));

  for (i = 0; i < nGram; i++) {
    //fprintf(stderr, "%dつめの辞書 : %s -> %s\n", i+1, gram[i][0], gram[i][1]);
    gram2[i] = my_split_str_plus(gram[i][1], &nGram2[i]);
    /*
    for (j = 0; j < nGram2[i]; j++) {
      fprintf(stderr, "%s\n", gram2[i][j]);
    }
    */
  }

  // ここから
  matrix = (char ***) malloc(sizeof(char **) * (n+1));
  for (i = 0; i < n+1; i++) {
    matrix[i] = (char **) malloc(sizeof(char *) * (n));
    for (j = 0; j < n; j++) {
      matrix[i][j] = (char *) malloc(sizeof(char) * (MAX_INPUT_LENGTH));
    }
  }

  // naname
  for (i = 0; i < n; i++) {
    for (j = 0; j < n; j++) {
      if(i == j) {
	for (k = 0; k < nDic1; k++) {
	  if (strcmp(dic1[k][0], words[j]) == 0) {
	    strcpy(matrix[i][j], dic1[k][1]);
	  }
	}
      } else {
	strcpy(matrix[i][j], "");
      }
    }
  }
  
  // naname1
  for (i = 0; i < n; i++) {
    for (j = 0; j < n; j++) {
      for (k = 0; k < nGram; k++) {
	if (nGram2[k] == 1) {
	  if(strcmp(gram2[k][0], matrix[i][j]) == 0){
	    strcpy(matrix[n][j], gram[k][0]);
	  }
	}
      }
    }
  }

  // naname2~
  for (i = 1; i < n; i++) {
    for (j = 0; j < n; j++) {
      for (k = 0; k < n; k++) {
	if (j + i == k) {
	  for (l = 0; l < nGram; l++) {
	    if (nGram2[l] == 2) {
	      for (m = 0; m < i; m++) {
		//fprintf(stderr, "(%d, %d):%s/(%d, %d):%s\n", j+m+1, k, matrix[j+m+1][k], j, k+m-i, matrix[j][k+m-i]);
		if((strcmp(matrix[j+m+1][k], gram2[l][1]) == 0) ||
		   (j+m+1 == k && strcmp(matrix[n][k], gram2[l][1]) == 0)) {
		  //fprintf(stderr, "!!!!!!!!!!!!!%s\n", gram[l][0]);
		  if((strcmp(matrix[j][k+m-i], gram2[l][0]) == 0) ||
		     (j == k+m-i && strcmp(matrix[n][k+m-i], gram2[l][0]) == 0)) {
		    //fprintf(stderr, "!!!!!!!!!!!!!%s\n", gram[l][0]);
		    strcpy(matrix[j][k], gram[l][0]);
		  }
		}
	      }
	    }
	  }
	  //fprintf(stderr, "(%d, %d):%s\n", i, j, matrix[i][j]);
	}
      }
    }
  }
  
  // print
  for (i = 0; i < n; i++) {
    for (j = 0; j < n; j++) {
      if (i == j && strcmp(matrix[n][j], "") != 0) {
	fprintf(stderr, "(%d, %d):%-4s/%-3s ", i, j, matrix[i][j], matrix[n][j]);
      } else if(i <= j) {
	fprintf(stderr, "(%d, %d):%-8s ", i, j, matrix[i][j]);
      } else {
	fprintf(stderr, "%-15s ", "");
      }
    }
    fprintf(stderr, "\n");
  }

  for (i = 0; i < nGram; i++) {
    for (j = 0; j < 2; j++) {
      free(gram[i][j]);
    }
    free(gram[i]);
  }
  free(gram);
  
  for (i = 0; i < nDic1; i++) {
    for (j = 0; j < 2; j++) {
      free(dic1[i][j]);
    }
    free(dic1[i]);
  }
  free(dic1);

  for (i = 0; i < n+1; i++) {
    for (j = 0; j < n; j++) {
      free(matrix[i][j]);
    }
    free(matrix[i]);
  }
  free(matrix);
  
  for (i = 0; i < n; i++) {
    free(words[i]);
  }
  free(words);

  return 0;
}
