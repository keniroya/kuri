#ifndef __MY_FUNC_H__
#define __MY_FUNC_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_STR_LENGTH 128
#define MAX_INPUT_LENGTH 256

char **my_split_str_blanks(char *, int *);
char **my_split_str_plus(char *, int *);
char ***scan_dictionary(char *, int *);

#endif
