CC = gcc
CFLAGS = -Wall -O3

all: exp1 exp2 exp3

exp1: exp1.c my_func.o
	$(CC) $(CFLAGS) -o $@ $^

exp2: exp2.c my_func.o
	$(CC) $(CFLAGS) -o $@ $^

exp3: exp3.c my_func.o
	$(CC) $(CFLAGS) -o $@ $^

my_func.o: my_func.c my_func.h
	$(CC) $(CFLAGS) -c my_func.c

clean:
	$(RM) *.o *~ *output* core exp1 exp2 exp3
